module.exports.session = {
  secret: '5b0bf43d9c63085eYehDilMangeMoreAndMore5613deec6089d092',
  cookie: {
    maxAge: 24 * 60 * 60 * 1000 * 7
  },
  adapter: 'redis', // https://github.com/visionmedia/connect-redis
  host: 'localhost',
  port: 6379,
  ttl: 86400,
// db: 0 This is not recommended. source :: https://groups.google.com/forum/#!topic/redis-db/vS5wX8X4Cjg
}
