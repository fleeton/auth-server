/**
 * VerificationController
 *
 * @description :: Server-side logic for managing Verifications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  email: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var token = req.param('token')

    return res.send(token, 200)
  },

  emailv: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var token = req.param('token')
    VerificationService.emailVerified(token, function (err, verified) {
      if (err)
        return res.send(err, 400)
      else
        return res.send(verified, 200)
    })
  }
}
