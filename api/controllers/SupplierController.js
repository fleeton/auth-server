/**
 * SupplierController
 *
 * @description :: Server-side logic for managing Suppliers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  signup: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var password = req.param('password')
    var name = req.param('name')
    var email = req.param('email')
    var phone = req.param('phone')

    if (!password) {
      return res.send({errorMessage: 'password missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!name) {
      return res.send({errorMessage: "You can't be nameless", errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!email) {
      return res.send({errorMessage: 'emial missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!phone) {
      return res.send({errorMessage: 'phone missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    LoginService.createSupplier(password, name, email, phone, function (err, supplier) {
      if (err) {
        return res.send(err, 400)
      }

      return res.send(supplier, 200)
    })
  },

  signin: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var email = req.param('email')
    var password = req.param('password')

    if (!email) {
      return res.send({errorMessage: 'email is missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!password) {
      return res.send({errorMessage: 'password missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    LoginService.supplierEntry(email, password, function (err, detail) {
      if (err) {
        return res.send(err, 400)
      }

      req.session.auth = true
      req.session.userId = detail.id
      req.session.userType = 'supplier'
      req.session.etime = new Date().toISOString()

      RedisService.handleCustomer(detail, req.session.etime, req.session.userType, function (err, redirectCode) {
        if (err) {
          res.redirect('/error')
        } else {
          req.session.acode = redirectCode
          var redir_
          if (next == 'f')
            redir_ = 'http://fleeton.net/auth?code=' + redirectCode
          else
            redir_ = 'http://test.transport.co.in:1337/auth?code=' + redirectCode

          res.redirect(redir_)
        }
      })
    })
  }
}
