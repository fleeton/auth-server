/**
 * CustomerController
 *
 * @description :: Server-side logic for managing Customers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  signup: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var password = req.param('password')
    var firstname = req.param('firstname')
    var lastname = req.param('lastname')
    var email = req.param('email')
    var phone = req.param('phone')

    if (!password) {
      return res.send({errorMessage: 'password missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!firstname) {
      return res.send({errorMessage: 'firstname is missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!lastname) {
      return res.send({errorMessage: 'lastname is missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!email) {
      return res.send({errorMessage: 'email is missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!phone) {
      return res.send({errorMessage: 'phone missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    // LoginService.createCustomer(username, password, firstname, lastname, email, phone, function(err, supplier){
    LoginService.createCustomer(password, firstname, lastname, email, phone, function (err, customer) {
      if (err) {
        return res.send(err, 500)
      }

      return res.send(customer, 200)
    })
  },

  signin: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    var email = req.param('email')
    var password = req.param('password')
    var next = req.param('next')

    if (!email) {
      return res.send({errorMessage: 'email is missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!password) {
      return res.send({errorMessage: 'password missing', errorCode: 'VALIDATION_ERROR'}, 400)
    }

    if (!next) {
      next = 'http://transport.co.in/'
    }

    LoginService.customerEntry(email, password, function (err, detail) {
      if (err) {
        return res.send(err, 400)
      }

      req.session.auth = true
      req.session.userId = detail.id
      req.session.userType = 'customer'
      req.session.etime = new Date().toISOString()

      RedisService.handleCustomer(detail, req.session.etime, req.session.userType, function (err, redirectCode) {
        if (err) {
          res.redirect('/error')
        } else {
          req.session.acode = redirectCode
          var redir_
          if (next == 'f')
            redir_ = 'http://fleeton.net/auth?code=' + redirectCode
          else
            redir_ = 'http://foo.transport.co.in:1338/auth?code=' + redirectCode

          res.redirect(redir_)
        }
      })
    })
  },

  // For test
  login: function (req, res) {
    sails.log('TIME: ' + new Date() + ': '
      + req.method + ' ' + req.headers.host + ' ' + req.url + ' '
      + JSON.stringify(req.params) + ' ' + req.route.params
    )

    return res.view('login')
  }
}
