var nodemailer = require('nodemailer')
var username = sails.config.mailgun.login
var password = sails.config.mailgun.pass

var transporter = nodemailer.createTransport({
  service: 'Mailgun',
  auth: {
    user: username,
    pass: password
  }
})

exports.fly = function (mailOpts, cb) {
  transporter.sendMail(mailOpts, function (err, response) {
    if (err) {
      return cb(err, null)
    } else {
      return cb(null, response)
    }
  })
}

exports.sendMail = function (mes, cb) {
  var mailOpts = {
    from: 'noreply@transport.co.in',
    to: 'webmaster@transport.co.in',
    subject: 'The app has been crashed!',
    text: mes,
    html: mes
  }

  NotificationService.fly(mailOpts, function (e, r) {
    return cb(e, r)
  })
}

exports.emailConfirmation = function (email, hash, cb) {
  var mailOpts = {
    from: 'noreply@transport.co.in',
    to: email,
    subject: 'Transport.co.in email confirmation',
    text: 'Confirm your email by clicking to this link http://localhost:1337/user/email/' + hash,
    html: 'Confirm your email by clicking to this link <a href="http://localhost:1337/user/email/' + hash + '">' + 'http://localhost:1337/user/email/' + hash
  }

  NotificationService.fly(mailOpts, function () {})
}

exports.sendConfirmationCode = function (phone, token, cb) {
  /* The phone messages */
}
