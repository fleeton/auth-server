var redis = require('redis');
var client = redis.createClient(port=6379, host="localhost");

client.auth("password", function(err){
  if(err){
    sails.log.error(err);
  }
})

client.on('error', function (err) {
  sails.log.error('Error ' + err)
})

exports.set = function (key, value, cb) {
  client.set(key, value, function(err, aset){
    if(err)
      return cb(err, null)
    else
      return cb(null, aset)
  })
}

exports.get = function (key, cb) {
  client.get(key, function (err, reply) {
    if (err)
      return cb(err, null)
    else{
      return cb(null, reply)
    }
  })
}

//TODO :: What is the point of symmetric key encryption if you can't go back. The only positive side is unique code generation.`
exports.handleCustomer = function(detail, etime, userType, cb){
  var userId = detail.id;
  EncryptionService.getKeys(1, function(err, keys){
    if(err){
      return cb(err, null);
    }

    EncryptionService.encrypt(userId, etime, function(enrUid){
      var code = keys[0] + enrUid.toString();
      var value = {
        user: detail,
        etime: etime,
        utype: userType,
        auth: true
      }
      value = JSON.stringify(value)
      RedisService.set(code, value, function(err, allset){
        if(err){
          sails.log.error("CRITICAL :: Cant set from redis!")
        }
      })

      cb(null, code)
    })
  })
}