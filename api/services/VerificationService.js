exports.emailVerified = function (token, cb) {
  Confirmation.findOne({utility: 0, code: token}).exec(function (err, u_c) {
    if (err) {
      sails.log.error(err)
      return cb(err, null)
    }

    if (!u_c) {
      return cb({errorMessage: 'Wrong or expired code', errorCode: 'NOT ALLOWED'}, null)
    } else {
      if (u_c.isCustomer) {
        Customer.findOne({id: u_c.userId}).exec(function (err, usr_c) {
          if (err) {
            sails.log.error(err)
            return cb(err, null)
          }

          if (!usr_c) {
            sails.log.error('Obsolate data present in database! custId=' + u_c.userId)
            return cb('Try again', null)
          }

          Customer.update({id: usr_c.id}, {emailVerified: true}).exec(function (err, usr_c_u) {
            if (err) {
              sails.log.error(err)
              return cb(err, null)
            }

            Confirmation.destroy({id: u_c.id}).exec(function (err, cnf) {
              if (err)
                sails.log.error('confirmation id=' + u_c.id + ' cannot be deleted')
            })

            return cb(null, usr_c_u)
          })
        })
      }
    }
  })
}
