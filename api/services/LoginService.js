var bcrypt = require('bcrypt')
var crypto = require('crypto')

exports.createSupplier = function (password, name, email, phone, cb) {
  Supplier.findOne({'or': [{ email: email }, { phone: phone} ]}).exec(function (err, supplier) {
    if (err) {
      sails.log.error(err)
      return cb(err, null)
    }

    if (supplier != null) {
      if (supplier.email == email)
        return cb({errorMessage: 'This email is already taken', errorCode: 'VALIDATION_ERROR'}, null)
      else if (supplier.phone == phone)
        return cb({errorMessage: 'Phone numver already registered', errorCode: 'VALIDATION_ERROR'}, null)
    }

    var newSupplier = {
      'password': password,
      'name': name,
      'email': email,
      'phone': phone
    }

    Supplier.create(newSupplier).exec(function (err, supplier) {
      if (err) {
        sails.log.error(err)
        return cb(err, null)
      }
      cb(null, supplier)

      crypto.randomBytes(16, function (err, buf) {
        if (err) {
          sails.log.error(err)
          return cb(err, null)
        }

        var token = buf.toString('hex')
        var newConfirmation = {
          'userId': supplier.id,
          'utility': 0,
          'code': token
        }
        Confirmation.create(newConfirmation).exec(function (err, conf) {
          if (err) {
            sails.log.info(err)
          } else {
            NotificationService.emailConfirmation(email, token, function (err, notice) {})
          }
        })
      })

      crypto.randomBytes(3, function (err, buf) {
        if (err) {
          sails.log.error(err)
          return cb(err, null)
        }

        var token = buf.toString('hex')
        var newConfirmation = {
          'userId': supplier.id,
          'utility': 1,
          'code': token,
          'isCustomer': false
        }
        Confirmation.create(newConfirmation).exec(function (err, conf) {
          if (err)
            sails.log.info(err)
          else
            NotificationService.sendConfirmationCode(phone, token, function (err, notice) {})
        })
      })
    })
  })
}

// exports.createCustomer = function(username, password, firstname, lastname, email, phone, cb){
exports.createCustomer = function (password, firstname, lastname, email, phone, cb) {
  Customer.findOne({'or': [{ email: email }, { phone: phone} ]}).exec(function (err, customer) {
    if (err) {
      sails.log.error(err)
      return cb(err, null)
    }

    if (customer != null) {
      if (customer.email == email)
        return cb({errorMessage: 'This email is already taken', errorCode: 'VALIDATION_ERROR'}, null)
      else if (customer.phone == phone)
        return cb({errorMessage: 'Phone numver already registered', errorCode: 'VALIDATION_ERROR'}, null)
    }

    var newCustomer = {
      'email': email,
      'phone': phone,
      'password': password,
      'firstname': firstname,
      'lastname': lastname
    }

    Customer.create(newCustomer).exec(function (err, customer) {
      if (err) {
        sails.log.error(err)
        return cb(err, null)
      }

      cb(null, customer)
      crypto.randomBytes(16, function (err, buf) {
        if (err) {
          sails.log.error(err)
          return cb(err, null)
        }

        var token = buf.toString('hex')
        var newConfirmation = {
          'userId': customer.id,
          'utility': 0,
          'code': token,
          'isCustomer': true
        }

        Confirmation.create(newConfirmation).exec(function (err, conf) {
          if (err) {
            sails.log.info(err)
          } else {
            NotificationService.emailConfirmation(email, token, function (err, notice) {})
          }
        })
      })

    })
  })
}

exports.customerEntry = function (email, password, cb) {
  Customer.findOne({email: email}).exec(function (err, customer) {
    if (err) {
      sails.log.error(err)
      return cb(err, null)
    }

    if (!customer) {
      return cb({errorMessage: 'No such email exists', errorCode: 'NO_SUCH_USER'}, null)
    }

    var hash = customer.password
    bcrypt.compare(password, hash, function (err, result) {
      if (err) {
        sails.log.error(err)
        return cb(err, null)
      }

      if (!result) {
        return cb({errorMessage: 'Wrong password', errorCode: 'ACCESS_DENIED'}, null)
      } else {
        delete customer.createdAt
        delete customer.updatedAt
        delete customer.password
        return cb(null, customer)
      }
    })
  })
}

exports.supplierEntry = function (email, password, cb) {
  Supplier.findOne({email: email}).exec(function (err, supplier) {
    if (err) {
      sails.log.error(err)
      return cb({errorMessage: err, errorCode: 'DATABASE_ERROR'}, null)
    }

    if (!supplier) {
      return cb({errorMessage: 'No such email', errorCode: 'NO_SUCH_USER'}, null)
    }

    var hash = supplier.password
    bcrypt.compare(password, hash, function (err, result) {
      if (err) {
        sails.log.error(err)
        return cb({errorMessage: err, errorCode: 'BCRYPT_ERROR'}, null)
      }

      if (!result) {
        return cb({errorMessage: 'Wrong password', errorCode: 'ACCESS_DENIED'}, null)
      } else {
        delete supplier.createdAt
        delete supplier.updatedAt
        delete supplier.password
        return cb(null, supplier)
      }
    })
  })
}
