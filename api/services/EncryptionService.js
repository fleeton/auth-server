var crypto = require('crypto'),
  algorithm = 'aes-256-ctr',
  password = 'T415$15$flee70n$pr0duc7$m4de6y$nkm4n$#8912(0'

exports.encrypt = function (text, etime, cb) {
  var cipher, crypted
  if (!etime)
    cipher = crypto.createCipher(algorithm, password)
  else
    cipher = crypto.createCipher(algorithm, etime)

  crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex')
  return cb(crypted)
}

exports.decrypt = function (text, etime, cb) {
  var decipher, dec
  if (!etime)
    decipher = crypto.createDecipher(algorithm, password)
  else
    decipher = crypto.createDecipher(algorithm, etime)

  dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8')
  return cb(dec)
}

exports.getKeys = function (count, cb) {
  crypto.randomBytes(8, function (err, buf) {
    if (err) {
      sails.log.error(err)
      return cb(err, null)
    }

    var token = buf.toString('hex')
    if (count <= 1) {
      return cb(null, [token])
    } else {
      crypto.randomBytes(8, function (err, buf) {
        if (err) {
          sails.log.error(err)
          return cb(err, null)
        }

        var token1 = buf.toString('hex')
        return cb(null, [token, token1])
      })
    }
  })
}
