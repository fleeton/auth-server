/**
 * notLoggedIn
 *
 * @module      :: Policy
 * @description :: Ensures user is not logged in`\
 * @TODO        :: The session has not been reset! and never destroyed! Not cool!
 */
module.exports = function (req, res, next) {
  if (!req.session.auth) {
    return next()
  } else {
    RedisService.get(req.session.acode, function (err, value) {
      if (err) {
        return res.forbidden('Some reeor occured! try again!')
      }

      if (!value || value == null) {
        req.session.destroy()
        return next()
      }

      else
        return res.forbidden('You are already logged in.')
    })
  }
}
